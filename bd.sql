-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-10-2018 a las 14:13:14
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `galery`
--
CREATE DATABASE IF NOT EXISTS `galery` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `galery`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images`
--

CREATE TABLE `images` (
  `images_id` int(11) NOT NULL,
  `images_title` varchar(255) NOT NULL,
  `images_desc` varchar(255) NOT NULL,
  `images_url` varchar(255) NOT NULL,
  `images_state` int(11) NOT NULL,
  `images_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
