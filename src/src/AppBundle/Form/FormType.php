<?php

namespace Val\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoadGaleryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vacCargGls')
            ->add('vacCargFecHra')
            ->add('vacCargEst')
            ->add('per')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Val\MainBundle\Entity\TperVacCarg'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'val_mainbundle_tpervaccarg';
    }
}
