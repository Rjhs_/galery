<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity
 */
class Images {

    /**
     * @var integer
     *
     * @ORM\Column(name="images_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imagesId;

    /**
     * @var string
     *
     * @ORM\Column(name="images_title", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Title should not be blank.")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    private $imagesTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="images_desc", type="string", length=255, nullable=false)
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Description cannot be longer than {{ limit }} characters"
     * )
     */
    private $imagesDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="images_url", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "URL should not be blank.")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "URL cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     */
    private $imagesUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="images_state", type="integer", nullable=false)
     * @Assert\NotBlank(message = "State should not be blank.")
     * @Assert\Choice(
     *     choices = { 1, 0 },
     *     message = "Choose a valid state."
     * )
     */

    private $imagesState = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="images_date", type="datetime", nullable=false)
     */
    private $imagesDate;

    /**
     * Get imagesId
     *
     * @return integer 
     */
    public function getImagesId() {
        return $this->imagesId;
    }

    /**
     * Set imagesTitle
     *
     * @param string $imagesTitle
     * @return Images
     */
    public function setImagesTitle($imagesTitle) {
        $this->imagesTitle = $imagesTitle;
        return $this;
    }

    /**
     * Get imagesTitle
     *
     * @return string 
     */
    public function getImagesTitle() {
        return $this->imagesTitle;
    }
    
    /**
     * Set imagesDesc
     *
     * @param string $imagesDesc
     * @return Images
     */
    public function setImagesDesc($imagesDesc) {
        $this->imagesDesc = $imagesDesc;
        return $this;
    }

    /**
     * Get imagesDesc
     *
     * @return string 
     */
    public function getImagesDesc() {
        return $this->imagesDesc;
    }

    /**
     * Set imagesUrl
     *
     * @param string $imagesUrl
     * @return Images
     */
    public function setImagesUrl($imagesUrl) {
        $this->imagesUrl = $imagesUrl;
        return $this;
    }

    /**
     * Get imagesUrl
     *
     * @return string 
     */
    public function getImagesUrl() {
        return $this->imagesUrl;
    }

    /**
     * Set imagesState
     *
     * @param integer $imagesState
     * @return Images
     */
    public function setImagesState($imagesState) {
        $this->imagesState = $imagesState;
        return $this;
    }

    /**
     * Get imagesState
     *
     * @return integer 
     */
    public function getImagesState() {
        return $this->imagesState;
    }

    /**
     * Set imagesDate
     *
     * @param Datatime $imagesDate
     * @return Images
     */
    public function setImagesDate($imagesDate) {
        $this->imagesDate = $imagesDate;
        return $this;
    }

    /**
     * Get imagesDate
     *
     * @return Datatime 
     */
    public function getImagesDate() {
        return $this->imagesDate;
    }

}
