<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GaleryController extends Controller {

    /**
     * @Route("/galery", name="galery")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('AppBundle:images')->findByImagesState(1);
        // replace this example code with whatever you need
        return $this->render('galery/index.html.twig', array(
                    'images' => $images,
        ));
    }

    /**
     * @Route("/galery/load", name="galery_load")
     */
    public function loadAction(Request $request) {
        // replace this example code with whatever you need

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('galery_load'))
                ->add('galeryUrl', 'text', [
                    'required' => true,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 200
                    ]
                ])
                ->add('galeryFile', 'file', array('data_class' => null, 'required' => false, 'mapped' => false))
                ->add('save', 'submit')
                ->getForm();

        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            $data = $request->get('data');
            $count = 0;
            foreach ($data as $key => $value) {
                if (count($value) === 3 && $key > 0) {

                    $image = new \AppBundle\Entity\Images();

                    if (isset($value[2]) && isset($value[1]) && isset($value[0])) {

                        $imagesTitle = $value[0];
                        $imagesDesc = $value[1];
                        $imagesUrl = $value[2];

                        if ($this->url_exists($imagesUrl)) {
                            $valid = array(".png", ".jpeg", ".jpg");
                            if (in_array($this->getExtension($imagesUrl), $valid)) {
                                $image->setImagesDesc($imagesDesc);
                                $image->setImagesTitle($imagesTitle);
                                $image->setImagesUrl($imagesUrl);
                                $image->setImagesDate(new \DateTime("now"));
                                $ex = $this->getDoctrine()->getManager();
                                $ex->persist($image);
                                $ex->flush();
                                $count++;
                            } else {
//                                echo $this->getExtension($imagesUrl);
                            }

//                            
                        }
                    }
                }
            }
            return new \Symfony\Component\HttpFoundation\JsonResponse(array('code' => 200, 'message' => "Se han procesado $count imagenes de " . count($data) . "."));
        }
        return $this->render('galery/load.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/galery/delete/{id}", name="galery_delete")
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('AppBundle:images')->findOneByImagesId($id);
        $em->remove($images);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
                'mensaje', 'It has been successfully deleted!'
        );
        return new \Symfony\Component\HttpFoundation\JsonResponse(array('passes' => true, 'message' => "It has been successfully deleted!"));
    }

    /**
     * @Route("/galery/edit/{id}", name="galery_edit")
     */
    public function editAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('AppBundle:images')->findOneByImagesId($id);
        $form = $this->createFormBuilder($images)
                ->setAction($this->generateUrl('galery_edit', array('id' => $id)))
                ->add('imagesTitle', 'text', [
                    'required' => true,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesDesc', 'text', [
                    'required' => false,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesUrl', 'text', [
                    'required' => true,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesState', 'choice', array(
                    'choices' => array(
                        'Active' => 1,
                        'Inactive' => 0
                    ),
                    // *this line is important*
                    'choices_as_values' => true,
                    'data' => $images->getImagesState()
                ))
                ->add('save', 'submit')
                ->getForm();
        $form->handleRequest($request);
        $errors = array();
        if ($request->isMethod('POST')) {
            $image = $form->getData();
            $validator = $this->get('validator');
            $errors = $validator->validate($images);
            if (count($errors) > 0) {
                return $this->render('galery/edit.html.twig', array('form' => $form->createView(), 'errors' => $errors));
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($image);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add(
                    'mensaje', 'It has been successfully edited!'
            );
            return $this->redirectToRoute('galery');
        }
        return $this->render('galery/edit.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    /**
     * @Route("/galery/add", name="galery_add")
     */
    public function addAction(Request $request) {
//        $em = $this->getDoctrine()->getManager();
//        $images = $em->getRepository('AppBundle:images')->findOneByImagesId($id);
        $images = new \AppBundle\Entity\Images();
        $form = $this->createFormBuilder($images)
                ->setAction($this->generateUrl('galery_add'))
                ->add('imagesTitle', 'text', [
                    'required' => true,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesDesc', 'text', [
                    'required' => false,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesUrl', 'text', [
                    'required' => true,
                    'mapped' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255
                    ]
                ])
                ->add('imagesState', 'choice', array(
                    'choices' => array(
                        'Active' => 1,
                        'Inactive' => 0
                    ),
                    // *this line is important*
                    'choices_as_values' => true,
                    'data' => $images->getImagesState()
                ))
                ->add('save', 'submit')
                ->getForm();
        $form->handleRequest($request);
        $errors = array();
        if ($request->isMethod('POST')) {
            $image = $form->getData();
            $validator = $this->get('validator');
            $errors = $validator->validate($images);
            if (count($errors) > 0) {
                return $this->render('galery/add.html.twig', array('form' => $form->createView(), 'errors' => $errors));
            }
            $entityManager = $this->getDoctrine()->getManager();
            $image->setImagesDate(new \DateTime("now"));
            $entityManager->persist($image);
            $entityManager->flush();
            $this->get('session')->getFlashBag()->add(
                    'mensaje', 'It has been successfully edited!'
            );
            return $this->redirectToRoute('galery');
        }
        return $this->render('galery/add.html.twig', array('form' => $form->createView(), 'errors' => $errors));
    }

    private function getspreadsheetId($string) {
        $h1 = strrpos($string, "/d/") + 3;
        $h2 = strlen(strrchr($string, "/"));
        $spreadsheetId = substr($string, $h1, -$h2);
        return $spreadsheetId;
    }

    private function url_exists($url) {
        $file_headers = get_headers($url);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
//        if (!$fp = curl_init($url))
//            return false;
        return $exists;
    }

    private function getExtension($string) {
//        $h1 = strrpos($string, "/d/") + 3;
        $h2 = strlen(strrchr($string, "."));
        $spreadsheetId = substr($string, -$h2);
        return $spreadsheetId;
    }

}
