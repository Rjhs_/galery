<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);

        // homepage
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_homepage;
            } else {
                return $this->redirect($rawPathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }
        not_homepage:

        if (0 === strpos($pathinfo, '/galery')) {
            // galery
            if ('/galery' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GaleryController::indexAction',  '_route' => 'galery',);
            }

            // galery_load
            if ('/galery/load' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GaleryController::loadAction',  '_route' => 'galery_load',);
            }

            // galery_delete
            if (0 === strpos($pathinfo, '/galery/delete') && preg_match('#^/galery/delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'galery_delete')), array (  '_controller' => 'AppBundle\\Controller\\GaleryController::deleteAction',));
            }

            // galery_edit
            if (0 === strpos($pathinfo, '/galery/edit') && preg_match('#^/galery/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'galery_edit')), array (  '_controller' => 'AppBundle\\Controller\\GaleryController::editAction',));
            }

            // galery_add
            if ('/galery/add' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\GaleryController::addAction',  '_route' => 'galery_add',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
