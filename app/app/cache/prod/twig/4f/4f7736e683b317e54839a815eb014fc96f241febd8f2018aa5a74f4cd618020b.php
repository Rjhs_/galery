<?php

/* :galery:load.html.twig */
class __TwigTemplate_b47ba2794389a1bc3443d5813d6169ed1c58c39da4c16166ef98dae7098a8a76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":galery:load.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_javascripts($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        function checkimage(url) {
            \$.ajax({
                url: url,
                type: 'HEAD',
                error: function ()
                {
                    return true;
                },
                success: function ()
                {
                    return true;
                }
            });
        }

        function makeApiCall() {
            var params = {
                // The ID of the spreadsheet to retrieve data from.
                spreadsheetId: '1PCrwoc4CAAMUpHbYOvd3DauPpyVZXhrpj4iQLndzj58', // TODO: Update placeholder value.

                // The A1 notation of the values to retrieve.
                range: 'Sheet 1', // TODO: Update placeholder value.

                // How values should be represented in the output.
                // The default render option is ValueRenderOption.FORMATTED_VALUE.
        ";
        // line 33
        echo "
                    // How dates, times, and durations should be represented in the output.
                    // This is ignored if value_render_option is
                    // FORMATTED_VALUE.
                    // The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
                    //dateTimeRenderOption: '',  // TODO: Update placeholder value.
                };
                var request = gapi.client.sheets.spreadsheets.values.get(params);
                request.then(function (response) {
                    var range = response.result;
                    if (range.values.length > 0) {
                        var url = '";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("galery_load");
        echo "';
                        \$.ajax({
                            url: url,
                            type: 'POST',
                            data: {'data': range.values},
                            success: function (data) {
                                \$(\"body\").removeClass(\"working\");
                                \$('#divMessages').empty();
                                var html = \"<div class='alert alert-success alert-dismissible res' role='alert'>\"
                                        + \"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\"
                                        + \"<p>\" + data.message + \"</p>\";
                                \$('#divMessages').append(html);
                            },
                            error: function () {

                            }
                        });

                        /* for (let i in range.values) {
                         
                         if (range.values[i].length === 3) {
                         
                         if (0 in range.values[i]) {
                         var item = range.values[i][0];
                         }
                         if (1 in range.values[i]) {
                         var des = range.values[i][1];
                         }
                         if (2 in range.values[i]) {
                         var img = range.values[i][2];
                         }
                         var html = \"<div class='col-md-4'>\" +
                         \"<div class='card mb-4 shadow-sm'>\" +
                         \"<img class='card-img-top' src='\" + img + \"' alt='\" + item + \" [100%x225]' style='height: 225px; width: 100%; display: block;' >\" +
                         \"<div class='card-body'>\" +
                         \"<p class='card-text'>\" + des + \"</p>\" +
                         \"<div class='d-flex justify-content-between align-items-center'>\" +
                         \"<div class='btn-group'>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>View</button>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>Edit</button>\" +
                         \"</div>\" +
                         \"<small class='text-muted'>9 mins</small>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\";
        ";
        // line 91
        echo "                         document.getElementById(\"gridPopulate\").innerHTML += html;
        ";
        // line 93
        echo "                         
                         
                         
                         }
                         } */

                    } else {
                        appendPre('No data found.');
                    }

                }, function (reason) {
                    console.error('error: ' + reason.result.error.message);
                });
            }

            function initClient() {
                var API_KEY = 'AIzaSyDxPP_jK8kTvVjAF7makXqtjZEYgkTi4fI'; // TODO: Update placeholder with desired API key.

                var CLIENT_ID = '193554620301-llcgqu3fu8muo7qmqf5fnks45gtklafb.apps.googleusercontent.com'; // TODO: Update placeholder with desired client ID.

                // TODO: Authorize using one of the following scopes:
                //   'https://www.googleapis.com/auth/drive'
                //   'https://www.googleapis.com/auth/drive.file'
                //   'https://www.googleapis.com/auth/drive.readonly'
                //   'https://www.googleapis.com/auth/spreadsheets'
                //   'https://www.googleapis.com/auth/spreadsheets.readonly'
                var SCOPE = 'https://www.googleapis.com/auth/spreadsheets.readonly';
                gapi.client.init({
                    'apiKey': API_KEY,
                    'clientId': CLIENT_ID,
                    'scope': SCOPE,
                    'discoveryDocs': ['https://sheets.googleapis.com/\$discovery/rest?version=v4'],
                }).then(function () {
                    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
                    updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                });
            }

            function handleClientLoad() {
                gapi.load('client:auth2', initClient);
            }

            function updateSignInStatus(isSignedIn) {
                if (isSignedIn) {
                    makeApiCall();
                }
            }

            function handleSignInClick(event) {
                gapi.auth2.getAuthInstance().signIn();
            }

            function handleSignOutClick(event) {
                gapi.auth2.getAuthInstance().signOut();
            }

            function load() {
        ";
        // line 151
        echo "                \$(\"body\").addClass(\"working\");
                handleClientLoad();
            }
    </script>
    <script async defer src=\"https://apis.google.com/js/api.js\"
            onreadystatechange=\"if (this.readyState === 'complete') this.onload()\">
    </script>

";
    }

    // line 162
    public function block_body($context, array $blocks = array())
    {
        echo "    

    <main role=\"main\">
        <div class=\"container\">
            <h1 class=\"jumbotron-heading\">Load Galery</h1>
            ";
        // line 168
        echo "            <div class=\"col-md-12\">
                ";
        // line 169
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "mensaje"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 170
            echo "                    <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 172
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 175
        echo "            </div>
            <div class=\"col-md-12\">
                ";
        // line 177
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 178
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 180
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 183
        echo "            </div>
            <div class=\"col-md-12\" id=\"divMessages\">
                ";
        // line 185
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 186
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 188
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "            </div>

            ";
        // line 193
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start');
        echo "            
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">URL</label>
                ";
        // line 196
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "galeryUrl", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">File</label>
                ";
        // line 200
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "galeryFile", array()), 'widget', array("attr" => array("class" => "form-control-file")));
        echo "
            </div>
            <div class=\"form-group\">
                ";
        // line 203
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
                <button type=\"button\" class=\"btn btn-success\" onclick=\"load();\">Load</button>
            </div>

            ";
        // line 207
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
        echo "


            <div class=\"album py-5 bg-light\">
                <div class=\"container\">
                    <div class=\"row\" id=\"gridPopulate\"></div>
                </div>
            </div>

        </div>


    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            ";
        // line 228
        echo "        </div>
    </footer>


";
    }

    public function getTemplateName()
    {
        return ":galery:load.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 228,  300 => 207,  293 => 203,  287 => 200,  280 => 196,  274 => 193,  270 => 191,  261 => 188,  257 => 186,  253 => 185,  249 => 183,  240 => 180,  236 => 178,  232 => 177,  228 => 175,  219 => 172,  215 => 170,  211 => 169,  208 => 168,  199 => 162,  187 => 151,  128 => 93,  125 => 91,  76 => 44,  63 => 33,  32 => 5,  29 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":galery:load.html.twig", "C:\\xampp\\htdocs\\galery\\app/Resources\\views/galery/load.html.twig");
    }
}
