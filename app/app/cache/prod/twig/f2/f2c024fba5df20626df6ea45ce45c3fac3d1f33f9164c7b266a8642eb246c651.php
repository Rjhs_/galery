<?php

/* :galery:index.html.twig */
class __TwigTemplate_17853f97ad8b35ac42d72d1d93bdeb7994b4765968780a4621a2b05448d35392 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":galery:index.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        function deleteImage(id) {
            var urlId = '";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("galery_delete", array("id" => "iddinamic"));
        echo "';

            var url = urlId.replace(\"iddinamic\", id);
        ";
        // line 10
        echo "        ";
        // line 11
        echo "                bootbox.confirm({
        ";
        // line 13
        echo "                    message: '¿ Are you sure to delete this image?',
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            \$(\"body\").addClass(\"working\");
                            \$.ajax({
                                url: url,
                                type: \"POST\",
                                data: '&accion=' + 'accion',
                                success: function (data) {
                                    \$(\"body\").removeClass(\"working\");
                                    if (data.passes) {
                                        location.reload();
                                    } else {
                                        alert(\"Ha ocurrido un error!\");
                                    }
                                }
                            });
                        }
                    }
                });
            }
    </script>
";
    }

    // line 46
    public function block_body($context, array $blocks = array())
    {
        // line 47
        echo "
    <main role=\"main\">

        <div class=\"album py-5 bg-light\">
            <div class=\"container\">
                <h1 class=\"jumbotron-heading\">Galery</h1>
                ";
        // line 54
        echo "                <div class=\"col-md-12\">
                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "mensaje"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 56
            echo "                        <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                            <p>";
            // line 58
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                </div>
                <div class=\"col-md-12\">
                    ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 64
            echo "                        <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                            <p>";
            // line 66
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "                </div>
                <div class=\"row\">
                    ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
            // line 72
            echo "                        <div class=\"col-md-4\">
                            <div class=\"card mb-4 shadow-sm\">
                                <img class=\"card-img-top\" src=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "imagesUrl", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "imagesTitle", array()), "html", null, true);
            echo "[100%x225]\" style=\"height: 225px; width: 100%; display: block;\" >
                                <div class=\"card-body\">
                                    <p class=\"card-text\">";
            // line 76
            echo $this->getAttribute($context["img"], "imagesDesc", array());
            echo "</p>
                                    <div class=\"d-flex justify-content-between align-items-center\">
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-sm btn-outline-secondary\" onclick=\"deleteImage(";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["img"], "imagesId", array()), "html", null, true);
            echo ")\">Delete</button>
                                            <a type=\"button\" class=\"btn btn-sm btn-outline-secondary\" href=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("galery_edit", array("id" => $this->getAttribute($context["img"], "imagesId", array()))), "html", null, true);
            echo "\">Edit</a>
                                        </div>
                                        <small class=\"text-muted\">";
            // line 82
            echo $this->getAttribute($context["img"], "imagesTitle", array());
            echo "</small>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "
                </div>
            </div>
        </div>

    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap? <a href=\"../../\">Visit the homepage</a> or read our <a href=\"../../getting-started/\">getting started guide</a>.</p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    ";
        // line 112
        echo "    ";
        // line 113
        echo "


";
    }

    public function getTemplateName()
    {
        return ":galery:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 113,  206 => 112,  184 => 88,  172 => 82,  167 => 80,  163 => 79,  157 => 76,  150 => 74,  146 => 72,  142 => 71,  138 => 69,  129 => 66,  125 => 64,  121 => 63,  117 => 61,  108 => 58,  104 => 56,  100 => 55,  97 => 54,  89 => 47,  86 => 46,  50 => 13,  47 => 11,  45 => 10,  39 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":galery:index.html.twig", "C:\\xampp\\htdocs\\galery\\app/Resources\\views/galery/index.html.twig");
    }
}
