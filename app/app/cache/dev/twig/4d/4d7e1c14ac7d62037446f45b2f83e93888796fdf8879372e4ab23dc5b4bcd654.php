<?php

/* :galery:load.html.twig */
class __TwigTemplate_553e335e95d895818c9f5323e8a2e40c3bf2fc7ec31704ccde3fd58b8e2dc229 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":galery:load.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":galery:load.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 5
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        function checkimage(url) {
            \$.ajax({
                url: url,
                type: 'HEAD',
                error: function ()
                {
                    return true;
                },
                success: function ()
                {
                    return true;
                }
            });
        }

        function makeApiCall() {
            var params = {
                // The ID of the spreadsheet to retrieve data from.
                spreadsheetId: '1PCrwoc4CAAMUpHbYOvd3DauPpyVZXhrpj4iQLndzj58', // TODO: Update placeholder value.

                // The A1 notation of the values to retrieve.
                range: 'Sheet 1', // TODO: Update placeholder value.

                // How values should be represented in the output.
                // The default render option is ValueRenderOption.FORMATTED_VALUE.
        ";
        // line 33
        echo "
                    // How dates, times, and durations should be represented in the output.
                    // This is ignored if value_render_option is
                    // FORMATTED_VALUE.
                    // The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
                    //dateTimeRenderOption: '',  // TODO: Update placeholder value.
                };
                var request = gapi.client.sheets.spreadsheets.values.get(params);
                request.then(function (response) {
                    var range = response.result;
                    if (range.values.length > 0) {
                        var url = '";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("galery_load");
        echo "';
                        \$.ajax({
                            url: url,
                            type: 'POST',
                            data: {'data': range.values},
                            success: function (data) {
                                \$(\"body\").removeClass(\"working\");
                                \$('#divMessages').empty();
                                var html = \"<div class='alert alert-success alert-dismissible res' role='alert'>\"
                                        + \"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\"
                                        + \"<p>\" + data.message + \"</p>\";
                                \$('#divMessages').append(html);
                            },
                            error: function () {

                            }
                        });

                        /* for (let i in range.values) {
                         
                         if (range.values[i].length === 3) {
                         
                         if (0 in range.values[i]) {
                         var item = range.values[i][0];
                         }
                         if (1 in range.values[i]) {
                         var des = range.values[i][1];
                         }
                         if (2 in range.values[i]) {
                         var img = range.values[i][2];
                         }
                         var html = \"<div class='col-md-4'>\" +
                         \"<div class='card mb-4 shadow-sm'>\" +
                         \"<img class='card-img-top' src='\" + img + \"' alt='\" + item + \" [100%x225]' style='height: 225px; width: 100%; display: block;' >\" +
                         \"<div class='card-body'>\" +
                         \"<p class='card-text'>\" + des + \"</p>\" +
                         \"<div class='d-flex justify-content-between align-items-center'>\" +
                         \"<div class='btn-group'>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>View</button>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>Edit</button>\" +
                         \"</div>\" +
                         \"<small class='text-muted'>9 mins</small>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\";
        ";
        // line 91
        echo "                         document.getElementById(\"gridPopulate\").innerHTML += html;
        ";
        // line 93
        echo "                         
                         
                         
                         }
                         } */

                    } else {
                        appendPre('No data found.');
                    }

                }, function (reason) {
                    console.error('error: ' + reason.result.error.message);
                });
            }

            function initClient() {
                var API_KEY = 'AIzaSyDxPP_jK8kTvVjAF7makXqtjZEYgkTi4fI'; // TODO: Update placeholder with desired API key.

                var CLIENT_ID = '193554620301-llcgqu3fu8muo7qmqf5fnks45gtklafb.apps.googleusercontent.com'; // TODO: Update placeholder with desired client ID.

                // TODO: Authorize using one of the following scopes:
                //   'https://www.googleapis.com/auth/drive'
                //   'https://www.googleapis.com/auth/drive.file'
                //   'https://www.googleapis.com/auth/drive.readonly'
                //   'https://www.googleapis.com/auth/spreadsheets'
                //   'https://www.googleapis.com/auth/spreadsheets.readonly'
                var SCOPE = 'https://www.googleapis.com/auth/spreadsheets.readonly';
                gapi.client.init({
                    'apiKey': API_KEY,
                    'clientId': CLIENT_ID,
                    'scope': SCOPE,
                    'discoveryDocs': ['https://sheets.googleapis.com/\$discovery/rest?version=v4'],
                }).then(function () {
                    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
                    updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                });
            }

            function handleClientLoad() {
                gapi.load('client:auth2', initClient);
            }

            function updateSignInStatus(isSignedIn) {
                if (isSignedIn) {
                    makeApiCall();
                }
            }

            function handleSignInClick(event) {
                gapi.auth2.getAuthInstance().signIn();
            }

            function handleSignOutClick(event) {
                gapi.auth2.getAuthInstance().signOut();
            }

            function load() {
        ";
        // line 151
        echo "                \$(\"body\").addClass(\"working\");
                handleClientLoad();
            }
    </script>
    <script async defer src=\"https://apis.google.com/js/api.js\"
            onreadystatechange=\"if (this.readyState === 'complete') this.onload()\">
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 162
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        echo "    

    <main role=\"main\">
        <div class=\"container\">
            <h1 class=\"jumbotron-heading\">Load Galery</h1>
            ";
        // line 168
        echo "            <div class=\"col-md-12\">
                ";
        // line 169
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "mensaje"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 170
            echo "                    <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 172
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 175
        echo "            </div>
            <div class=\"col-md-12\">
                ";
        // line 177
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 178
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 180
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 183
        echo "            </div>
            <div class=\"col-md-12\" id=\"divMessages\">
                ";
        // line 185
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 186
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 188
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "            </div>

            ";
        // line 193
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "            
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">URL</label>
                ";
        // line 196
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "galeryUrl", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">File</label>
                ";
        // line 200
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "galeryFile", array()), 'widget', array("attr" => array("class" => "form-control-file")));
        echo "
            </div>
            <div class=\"form-group\">
                ";
        // line 203
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
                <button type=\"button\" class=\"btn btn-success\" onclick=\"load();\">Load</button>
            </div>

            ";
        // line 207
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "


            <div class=\"album py-5 bg-light\">
                <div class=\"container\">
                    <div class=\"row\" id=\"gridPopulate\"></div>
                </div>
            </div>

        </div>


    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            ";
        // line 228
        echo "        </div>
    </footer>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":galery:load.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 228,  315 => 207,  308 => 203,  302 => 200,  295 => 196,  289 => 193,  285 => 191,  276 => 188,  272 => 186,  268 => 185,  264 => 183,  255 => 180,  251 => 178,  247 => 177,  243 => 175,  234 => 172,  230 => 170,  226 => 169,  223 => 168,  211 => 162,  196 => 151,  137 => 93,  134 => 91,  85 => 44,  72 => 33,  41 => 5,  35 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


{% block javascripts %}
    {{ parent() }}
    <script>
        function checkimage(url) {
            \$.ajax({
                url: url,
                type: 'HEAD',
                error: function ()
                {
                    return true;
                },
                success: function ()
                {
                    return true;
                }
            });
        }

        function makeApiCall() {
            var params = {
                // The ID of the spreadsheet to retrieve data from.
                spreadsheetId: '1PCrwoc4CAAMUpHbYOvd3DauPpyVZXhrpj4iQLndzj58', // TODO: Update placeholder value.

                // The A1 notation of the values to retrieve.
                range: 'Sheet 1', // TODO: Update placeholder value.

                // How values should be represented in the output.
                // The default render option is ValueRenderOption.FORMATTED_VALUE.
        {#                valueRenderOption: 'FORMATTED_VALUE'  // TODO: Update placeholder value.#}

                    // How dates, times, and durations should be represented in the output.
                    // This is ignored if value_render_option is
                    // FORMATTED_VALUE.
                    // The default dateTime render option is [DateTimeRenderOption.SERIAL_NUMBER].
                    //dateTimeRenderOption: '',  // TODO: Update placeholder value.
                };
                var request = gapi.client.sheets.spreadsheets.values.get(params);
                request.then(function (response) {
                    var range = response.result;
                    if (range.values.length > 0) {
                        var url = '{{ path('galery_load') }}';
                        \$.ajax({
                            url: url,
                            type: 'POST',
                            data: {'data': range.values},
                            success: function (data) {
                                \$(\"body\").removeClass(\"working\");
                                \$('#divMessages').empty();
                                var html = \"<div class='alert alert-success alert-dismissible res' role='alert'>\"
                                        + \"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\"
                                        + \"<p>\" + data.message + \"</p>\";
                                \$('#divMessages').append(html);
                            },
                            error: function () {

                            }
                        });

                        /* for (let i in range.values) {
                         
                         if (range.values[i].length === 3) {
                         
                         if (0 in range.values[i]) {
                         var item = range.values[i][0];
                         }
                         if (1 in range.values[i]) {
                         var des = range.values[i][1];
                         }
                         if (2 in range.values[i]) {
                         var img = range.values[i][2];
                         }
                         var html = \"<div class='col-md-4'>\" +
                         \"<div class='card mb-4 shadow-sm'>\" +
                         \"<img class='card-img-top' src='\" + img + \"' alt='\" + item + \" [100%x225]' style='height: 225px; width: 100%; display: block;' >\" +
                         \"<div class='card-body'>\" +
                         \"<p class='card-text'>\" + des + \"</p>\" +
                         \"<div class='d-flex justify-content-between align-items-center'>\" +
                         \"<div class='btn-group'>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>View</button>\" +
                         \"<button type='button' class='btn btn-sm btn-outline-secondary'>Edit</button>\" +
                         \"</div>\" +
                         \"<small class='text-muted'>9 mins</small>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\" +
                         \"</div>\";
        {#                                if (checkimage(img)) {#}
                         document.getElementById(\"gridPopulate\").innerHTML += html;
        {#                                }#}
                         
                         
                         
                         }
                         } */

                    } else {
                        appendPre('No data found.');
                    }

                }, function (reason) {
                    console.error('error: ' + reason.result.error.message);
                });
            }

            function initClient() {
                var API_KEY = 'AIzaSyDxPP_jK8kTvVjAF7makXqtjZEYgkTi4fI'; // TODO: Update placeholder with desired API key.

                var CLIENT_ID = '193554620301-llcgqu3fu8muo7qmqf5fnks45gtklafb.apps.googleusercontent.com'; // TODO: Update placeholder with desired client ID.

                // TODO: Authorize using one of the following scopes:
                //   'https://www.googleapis.com/auth/drive'
                //   'https://www.googleapis.com/auth/drive.file'
                //   'https://www.googleapis.com/auth/drive.readonly'
                //   'https://www.googleapis.com/auth/spreadsheets'
                //   'https://www.googleapis.com/auth/spreadsheets.readonly'
                var SCOPE = 'https://www.googleapis.com/auth/spreadsheets.readonly';
                gapi.client.init({
                    'apiKey': API_KEY,
                    'clientId': CLIENT_ID,
                    'scope': SCOPE,
                    'discoveryDocs': ['https://sheets.googleapis.com/\$discovery/rest?version=v4'],
                }).then(function () {
                    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
                    updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                });
            }

            function handleClientLoad() {
                gapi.load('client:auth2', initClient);
            }

            function updateSignInStatus(isSignedIn) {
                if (isSignedIn) {
                    makeApiCall();
                }
            }

            function handleSignInClick(event) {
                gapi.auth2.getAuthInstance().signIn();
            }

            function handleSignOutClick(event) {
                gapi.auth2.getAuthInstance().signOut();
            }

            function load() {
        {#            this.onload = function () {};#}
                \$(\"body\").addClass(\"working\");
                handleClientLoad();
            }
    </script>
    <script async defer src=\"https://apis.google.com/js/api.js\"
            onreadystatechange=\"if (this.readyState === 'complete') this.onload()\">
    </script>

{% endblock %}


{% block body %}    

    <main role=\"main\">
        <div class=\"container\">
            <h1 class=\"jumbotron-heading\">Load Galery</h1>
            {# MENSAJES #}
            <div class=\"col-md-12\">
                {% for flashMessage in app.session.flashbag.get('mensaje') %}
                    <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flashMessage }}</p>
                    </div>
                {% endfor %}
            </div>
            <div class=\"col-md-12\">
                {% for flashMessage in app.session.flashbag.get('error') %}
                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flashMessage }}</p>
                    </div>
                {% endfor %}
            </div>
            <div class=\"col-md-12\" id=\"divMessages\">
                {% for flashMessage in app.session.flashbag.get('error') %}
                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flashMessage }}</p>
                    </div>
                {% endfor %}
            </div>

            {{ form_start(form) }}            
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">URL</label>
                {{ form_widget(form.galeryUrl , { 'attr': { 'class': 'form-control col-sm-10'}}) }}
            </div>
            <div class=\"form-group\">
                <label for=\"exampleFormControlFile1\">File</label>
                {{ form_widget(form.galeryFile , { 'attr': { 'class': 'form-control-file'}}) }}
            </div>
            <div class=\"form-group\">
                {{ form_widget(form.save , { 'attr': { 'class': 'btn btn-primary'}}) }}
                <button type=\"button\" class=\"btn btn-success\" onclick=\"load();\">Load</button>
            </div>

            {{ form_end(form) }}


            <div class=\"album py-5 bg-light\">
                <div class=\"container\">
                    <div class=\"row\" id=\"gridPopulate\"></div>
                </div>
            </div>

        </div>


    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            {#<p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap? <a href=\"../../\">Visit the homepage</a> or read our <a href=\"../../getting-started/\">getting started guide</a>.</p>#}
        </div>
    </footer>


{% endblock %}", ":galery:load.html.twig", "C:\\xampp\\htdocs\\galery\\app/Resources\\views/galery/load.html.twig");
    }
}
