<?php

/* :galery:add.html.twig */
class __TwigTemplate_54afee1cbb4f97e68c7ae4c073dca915020cd1f2770fe38d12c8432648d1c0c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":galery:add.html.twig", 1);
        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":galery:add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        echo "    

    <main role=\"main\">
        <div class=\"container\">
            <h1 class=\"jumbotron-heading\">Add Image</h1>
            ";
        // line 14
        echo "            <div class=\"col-md-12\">
                ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "mensaje"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 16
            echo "                    <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 18
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "            </div>
            <div class=\"col-md-12\">
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 24
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 26
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </div>
            <div class=\"col-md-12\" id=\"divMessages\">
                ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 32
            echo "                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "            </div>

            ";
        // line 39
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "      
            <div class=\"form-group\">
                <label for=\"imagesTitle\">Title</label>
                ";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imagesTitle", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>
            <div class=\"form-group\">
                <label for=\"imagesDesc\">Description</label>
                ";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imagesDesc", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>
            <div class=\"form-group\">
                <label for=\"imagesUrl\">URL</label>
                ";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imagesUrl", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>
            <div class=\"form-group\">
                <label for=\"imagesState\">State</label>
                ";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imagesState", array()), 'widget', array("attr" => array("class" => "form-control col-sm-10")));
        echo "
            </div>

            <div class=\"form-group\">
                ";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
                <button type=\"button\" class=\"btn btn-danger\">Cancel</button>
            </div>

            ";
        // line 62
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "


            <div class=\"album py-5 bg-light\">
                <div class=\"container\">
                    <div class=\"row\" id=\"gridPopulate\"></div>
                </div>
            </div>

        </div>


    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            ";
        // line 83
        echo "        </div>
    </footer>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return ":galery:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 83,  165 => 62,  158 => 58,  151 => 54,  144 => 50,  137 => 46,  130 => 42,  124 => 39,  120 => 37,  111 => 34,  107 => 32,  103 => 31,  99 => 29,  90 => 26,  86 => 24,  82 => 23,  78 => 21,  69 => 18,  65 => 16,  61 => 15,  58 => 14,  46 => 8,  35 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


{% block javascripts %}
{% endblock %}


{% block body %}    

    <main role=\"main\">
        <div class=\"container\">
            <h1 class=\"jumbotron-heading\">Add Image</h1>
            {# MENSAJES #}
            <div class=\"col-md-12\">
                {% for flashMessage in app.session.flashbag.get('mensaje') %}
                    <div class=\"alert alert-success alert-dismissible res\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flashMessage }}</p>
                    </div>
                {% endfor %}
            </div>
            <div class=\"col-md-12\">
                {% for flashMessage in app.session.flashbag.get('error') %}
                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flashMessage }}</p>
                    </div>
                {% endfor %}
            </div>
            <div class=\"col-md-12\" id=\"divMessages\">
                {% for error in errors %}
                    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ error.message }}</p>
                    </div>
                {% endfor %}
            </div>

            {{ form_start(form) }}      
            <div class=\"form-group\">
                <label for=\"imagesTitle\">Title</label>
                {{ form_widget(form.imagesTitle , { 'attr': { 'class': 'form-control col-sm-10'}}) }}
            </div>
            <div class=\"form-group\">
                <label for=\"imagesDesc\">Description</label>
                {{ form_widget(form.imagesDesc , { 'attr': { 'class': 'form-control col-sm-10'}}) }}
            </div>
            <div class=\"form-group\">
                <label for=\"imagesUrl\">URL</label>
                {{ form_widget(form.imagesUrl , { 'attr': { 'class': 'form-control col-sm-10'}}) }}
            </div>
            <div class=\"form-group\">
                <label for=\"imagesState\">State</label>
                {{ form_widget(form.imagesState , { 'attr': { 'class': 'form-control col-sm-10'}}) }}
            </div>

            <div class=\"form-group\">
                {{ form_widget(form.save , { 'attr': { 'class': 'btn btn-primary'}}) }}
                <button type=\"button\" class=\"btn btn-danger\">Cancel</button>
            </div>

            {{ form_end(form) }}


            <div class=\"album py-5 bg-light\">
                <div class=\"container\">
                    <div class=\"row\" id=\"gridPopulate\"></div>
                </div>
            </div>

        </div>


    </main>

    <footer class=\"text-muted\">
        <div class=\"container\">
            <p class=\"float-right\">
                <a href=\"#\">Back to top</a>
            </p>
            {#<p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap? <a href=\"../../\">Visit the homepage</a> or read our <a href=\"../../getting-started/\">getting started guide</a>.</p>#}
        </div>
    </footer>


{% endblock %}", ":galery:add.html.twig", "C:\\xampp\\htdocs\\galery\\app/Resources\\views/galery/add.html.twig");
    }
}
