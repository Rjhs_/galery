<?php

/* UserBundle:User:view.html.twig */
class __TwigTemplate_3e699627d27af7344376d937e8041cdf6e7981a0e74b9fd52939da5d89fa7604 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "UserBundle:User:view.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UserBundle:User:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
\t<div class=\"container well\">
\t    <div class=\"col-md-9\">
\t    <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "lastName", array()), "html", null, true);
        echo "</h2>
\t        <br>
\t        <dl>
\t            <dt>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Username"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("First name"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "firstName", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Last name"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "lastName", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Email"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Role"), "html", null, true);
        echo "</dt>
\t            <dd>
                \t";
        // line 40
        if (($this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "role", array()) == "ROLE_ADMIN")) {
            // line 41
            echo "\t\t\t\t\t\t";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Administrator", array(), "messages");
            // line 42
            echo "\t\t\t\t\t";
        } elseif (($this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "role", array()) == "ROLE_USER")) {
            // line 43
            echo "\t\t\t\t\t\t";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("User", array(), "messages");
            // line 44
            echo "                \t";
        }
        // line 45
        echo "\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Active"), "html", null, true);
        echo "</dt>
\t            <dd>
                \t";
        // line 51
        if (($this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "isActive", array()) == 1)) {
            // line 52
            echo "\t\t\t\t\t\t<span class=\"text-success\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Enabled", array(), "messages");
            echo "</span>
\t\t\t\t\t";
        } elseif (($this->getAttribute(        // line 53
($context["user"] ?? $this->getContext($context, "user")), "isActive", array()) == 0)) {
            // line 54
            echo "\t\t\t\t\t\t<span class=\"text-warning\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Disabled", array(), "messages");
            echo "</span>
                \t";
        }
        // line 56
        echo "\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Created"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 62
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "createdAt", array()), "d-m-Y H:i"), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Updated"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 69
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "updatedAt", array()), "d-m-Y H:i"), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>
\t        </dl>
\t    </div>
\t    <div class=\"col-md-3\">
\t    \t<h3>";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
        echo ":</h3>
\t\t\t<p>
\t\t    \t<a href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("emm_user_edit", array("id" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary btn-lg btn-block\">
\t\t    \t\t<span class=\"glyphicon glyphicon-edit\"></span>
\t\t        \t";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit user"), "html", null, true);
        echo "
\t\t\t\t </a>
\t\t\t</p>
\t\t\t<p>
\t\t\t    ";
        // line 84
        echo twig_include($this->env, $context, "EMMUserBundle:User:forms/form.html.twig", array("form" => ($context["delete_form"] ?? $this->getContext($context, "delete_form")), "message" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Are you sure ?")));
        echo "
\t\t\t</p>
\t    </div>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "UserBundle:User:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 84,  196 => 80,  191 => 78,  186 => 76,  176 => 69,  171 => 67,  163 => 62,  158 => 60,  152 => 56,  146 => 54,  144 => 53,  139 => 52,  137 => 51,  132 => 49,  126 => 45,  123 => 44,  120 => 43,  117 => 42,  114 => 41,  112 => 40,  107 => 38,  99 => 33,  94 => 31,  86 => 26,  81 => 24,  73 => 19,  68 => 17,  60 => 12,  55 => 10,  47 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
\t{{ parent() }}
\t<div class=\"container well\">
\t    <div class=\"col-md-9\">
\t    <h2>{{ user.firstName }} {{ user.lastName }}</h2>
\t        <br>
\t        <dl>
\t            <dt>{{'Username'|trans}}</dt>
\t            <dd>
\t                {{ user.username }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'First name'|trans}}</dt>
\t            <dd>
\t                {{ user.firstName }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Last name'|trans}}</dt>
\t            <dd>
\t                {{ user.lastName }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Email'|trans}}</dt>
\t            <dd>
\t                {{ user.email }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Role'|trans}}</dt>
\t            <dd>
                \t{% if user.role == 'ROLE_ADMIN' %}
\t\t\t\t\t\t{% trans %}Administrator{% endtrans %}
\t\t\t\t\t{% elseif user.role == 'ROLE_USER' %}
\t\t\t\t\t\t{% trans %}User{% endtrans %}
                \t{% endif %}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Active'|trans}}</dt>
\t            <dd>
                \t{% if user.isActive == 1 %}
\t\t\t\t\t\t<span class=\"text-success\">{% trans %}Enabled{% endtrans %}</span>
\t\t\t\t\t{% elseif user.isActive == 0 %}
\t\t\t\t\t\t<span class=\"text-warning\">{% trans %}Disabled{% endtrans %}</span>
                \t{% endif %}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Created'|trans}}</dt>
\t            <dd>
\t                {{ user.createdAt|date('d-m-Y H:i') }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{'Updated'|trans}}</dt>
\t            <dd>
\t                {{ user.updatedAt|date('d-m-Y H:i') }}
\t                &nbsp;
\t            </dd>
\t            <br>
\t        </dl>
\t    </div>
\t    <div class=\"col-md-3\">
\t    \t<h3>{{ 'Actions'|trans }}:</h3>
\t\t\t<p>
\t\t    \t<a href=\"{{ path('emm_user_edit', { id: user.id }) }}\" class=\"btn btn-primary btn-lg btn-block\">
\t\t    \t\t<span class=\"glyphicon glyphicon-edit\"></span>
\t\t        \t{{ 'Edit user'|trans }}
\t\t\t\t </a>
\t\t\t</p>
\t\t\t<p>
\t\t\t    {{ include('EMMUserBundle:User:forms/form.html.twig', { form: delete_form, message: 'Are you sure ?'|trans}) }}
\t\t\t</p>
\t    </div>
\t</div>
{% endblock %}", "UserBundle:User:view.html.twig", "C:\\xampp\\htdocs\\galery\\src\\UserBundle/Resources/views/User/view.html.twig");
    }
}
