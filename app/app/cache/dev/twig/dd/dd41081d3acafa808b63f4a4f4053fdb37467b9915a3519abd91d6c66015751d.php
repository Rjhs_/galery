<?php

/* UserBundle:User:index.html.twig */
class __TwigTemplate_cfcaf2405546588f96c52718d4aa98eaee494dbaebff444f453f1e08b05dda22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "UserBundle:User:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UserBundle:User:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

    <div class=\"container\">
        <div class=\"col-md-12\">
            <div class=\"page-header margin-none\">
                <h2 class=\"padding-none\"> Users </h2>
            </div>
            <div class=\"table-responsive\">

                <br>\t\t\t\t\t
                ";
        // line 14
        if ( !twig_length_filter($this->env, ($context["users"] ?? $this->getContext($context, "users")))) {
            // line 15
            echo "                    <h2>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No search results"), "html", null, true);
            echo "</h2>
                ";
        }
        // line 17
        echo "
                ";
        // line 18
        if (twig_length_filter($this->env, ($context["users"] ?? $this->getContext($context, "users")))) {
            // line 19
            echo "                    <table class=\"table table-striped table-hover\">
                        <thead>
                            <tr>
                                <th> Username</th>
                                <th> First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Created</th>
                                <th>Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 33
                echo "                                <tr data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
                echo "\">
                                    <td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
                echo "</td>
                                    <td>";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "firstName", array()), "html", null, true);
                echo "</td>
                                    <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastName", array()), "html", null, true);
                echo "</td>
                                    <td>";
                // line 37
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
                echo "</td>
                                    <td>
                                        ";
                // line 39
                if (($this->getAttribute($context["user"], "role", array()) == "ROLE_ADMIN")) {
                    // line 40
                    echo "                                            <strong>
                                                Administrator 
                                            </strong>
                                        ";
                } elseif (($this->getAttribute(                // line 43
$context["user"], "role", array()) == "ROLE_USER")) {
                    // line 44
                    echo "                                            <strong>
                                                User 
                                            </strong>
                                        ";
                }
                // line 48
                echo "                                    </td>
                                    <td>";
                // line 49
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "createdAt", array()), "d-m-Y H:i"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 50
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "updatedAt", array()), "d-m-Y H:i"), "html", null, true);
                echo "</td>                                    
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                        </tbody>
                    </table>
                ";
        }
        // line 56
        echo "
            </div>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "UserBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 56,  139 => 53,  130 => 50,  126 => 49,  123 => 48,  117 => 44,  115 => 43,  110 => 40,  108 => 39,  103 => 37,  99 => 36,  95 => 35,  91 => 34,  86 => 33,  82 => 32,  67 => 19,  65 => 18,  62 => 17,  56 => 15,  54 => 14,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    {{ parent() }}

    <div class=\"container\">
        <div class=\"col-md-12\">
            <div class=\"page-header margin-none\">
                <h2 class=\"padding-none\"> Users </h2>
            </div>
            <div class=\"table-responsive\">

                <br>\t\t\t\t\t
                {% if not users|length %}
                    <h2>{{ 'No search results'|trans}}</h2>
                {% endif %}

                {% if users|length %}
                    <table class=\"table table-striped table-hover\">
                        <thead>
                            <tr>
                                <th> Username</th>
                                <th> First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Created</th>
                                <th>Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for user in users %}
                                <tr data-id=\"{{ user.id }}\">
                                    <td>{{ user.username }}</td>
                                    <td>{{ user.firstName }}</td>
                                    <td>{{ user.lastName }}</td>
                                    <td>{{ user.email }}</td>
                                    <td>
                                        {% if user.role == 'ROLE_ADMIN' %}
                                            <strong>
                                                Administrator 
                                            </strong>
                                        {% elseif user.role == 'ROLE_USER' %}
                                            <strong>
                                                User 
                                            </strong>
                                        {% endif %}
                                    </td>
                                    <td>{{ user.createdAt|date('d-m-Y H:i') }}</td>
                                    <td>{{ user.updatedAt|date('d-m-Y H:i') }}</td>                                    
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>
                {% endif %}

            </div>
        </div>
    </div>


{% endblock %}
", "UserBundle:User:index.html.twig", "C:\\xampp\\htdocs\\galery\\src\\UserBundle\\Resources\\views\\User\\index.html.twig");
    }
}
