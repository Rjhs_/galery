<?php

/* UserBundle:Task:view.html.twig */
class __TwigTemplate_95de04e85a380a8418153b2d0b4cd148edd8d26f4a9fb3bc02a542b00ce27276 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "UserBundle:Task:view.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UserBundle:Task:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t";
        $this->displayParentBlock("body", $context, $blocks);
        echo "
\t<div class=\"container well\">
\t    <div class=\"col-md-9\">
\t    <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "title", array()), "html", null, true);
        echo "</h2>
\t        <br>
\t        <dl>
\t            <dt>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Description"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "description", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("User"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "fullName", array()), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Status"), "html", null, true);
        echo "</dt>
\t            <dd>
                \t";
        // line 26
        if (($this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "status", array()) == 0)) {
            // line 27
            echo "                \t    <span class=\"text-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Waiting"), "html", null, true);
            echo "</span>
                \t";
        } elseif (($this->getAttribute(        // line 28
($context["task"] ?? $this->getContext($context, "task")), "status", array()) == 1)) {
            // line 29
            echo "                \t    <span class=\"text-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Finish"), "html", null, true);
            echo "</span>
                \t";
        }
        // line 31
        echo "\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Created"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 37
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "createdAt", array()), "d-m-Y H:i"), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Updated"), "html", null, true);
        echo "</dt>
\t            <dd>
\t                ";
        // line 44
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "updatedAt", array()), "d-m-Y H:i"), "html", null, true);
        echo "
\t                &nbsp;
\t            </dd>
\t            <br>
\t        </dl>
\t    </div>
\t    ";
        // line 50
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 51
            echo "\t    <div class=\"col-md-3\">
\t        <h3>";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Actions"), "html", null, true);
            echo ":</h3>
\t        <p>
\t            <a href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("emm_task_edit", array("id" => $this->getAttribute(($context["task"] ?? $this->getContext($context, "task")), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-lg btn-block\">
\t                <span class=\"glyphicon glyphicon-edit\"></span>
\t                ";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Edit task"), "html", null, true);
            echo "
\t            </a>
\t        </p>
\t        <p>
\t            ";
            // line 60
            echo twig_include($this->env, $context, "EMMUserBundle:Task:forms/form.html.twig", array("form" => ($context["delete_form"] ?? $this->getContext($context, "delete_form")), "message" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Are you sure ?")));
            echo "
\t        </p>
\t    </div>
\t    ";
        }
        // line 64
        echo "\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "UserBundle:Task:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 64,  154 => 60,  147 => 56,  142 => 54,  137 => 52,  134 => 51,  132 => 50,  123 => 44,  118 => 42,  110 => 37,  105 => 35,  99 => 31,  93 => 29,  91 => 28,  86 => 27,  84 => 26,  79 => 24,  71 => 19,  66 => 17,  58 => 12,  53 => 10,  47 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% block body %}
\t{{ parent() }}
\t<div class=\"container well\">
\t    <div class=\"col-md-9\">
\t    <h2>{{ task.title }}</h2>
\t        <br>
\t        <dl>
\t            <dt>{{ 'Description'|trans }}</dt>
\t            <dd>
\t                {{ task.description }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{ 'User'|trans }}</dt>
\t            <dd>
\t                {{ user.fullName }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{ 'Status'|trans }}</dt>
\t            <dd>
                \t{% if task.status == 0 %}
                \t    <span class=\"text-danger\">{{ 'Waiting'|trans }}</span>
                \t{% elseif task.status == 1 %}
                \t    <span class=\"text-success\">{{ 'Finish'|trans }}</span>
                \t{% endif %}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{ 'Created'|trans }}</dt>
\t            <dd>
\t                {{ task.createdAt|date('d-m-Y H:i') }}
\t                &nbsp;
\t            </dd>
\t            <br>

\t            <dt>{{ 'Updated'|trans }}</dt>
\t            <dd>
\t                {{ task.updatedAt|date('d-m-Y H:i') }}
\t                &nbsp;
\t            </dd>
\t            <br>
\t        </dl>
\t    </div>
\t    {% if is_granted('ROLE_ADMIN') %}
\t    <div class=\"col-md-3\">
\t        <h3>{{ 'Actions'|trans }}:</h3>
\t        <p>
\t            <a href=\"{{ path('emm_task_edit', {id: task.id}) }}\" class=\"btn btn-primary btn-lg btn-block\">
\t                <span class=\"glyphicon glyphicon-edit\"></span>
\t                {{ 'Edit task'|trans }}
\t            </a>
\t        </p>
\t        <p>
\t            {{ include('EMMUserBundle:Task:forms/form.html.twig', { form: delete_form, message: 'Are you sure ?'|trans }) }}
\t        </p>
\t    </div>
\t    {% endif %}
\t</div>
{% endblock %}", "UserBundle:Task:view.html.twig", "C:\\xampp\\htdocs\\galery\\src\\UserBundle/Resources/views/Task/view.html.twig");
    }
}
